# Groupe et présentation du projet
 
 Notre projet final abordera comme problématique : **Comment aider les personnes atteintes de tremblements chroniques à retrouver une autonomie au quotidien ?**

## Présentation de l'équipe 

![Alt text](docs/images/398282540_1086659932492703_7191071345750358704_n.jpg)

 - [Merlin Pierre](https://merlin-pierre-fablab-ulb-enseignements-2023-2024-36815b92ca81ce.gitlab.io/)
 - [Noa Lipmanowicz](https://noa-lipmanowicz-fablab-ulb-enseignements-2023-20-cba683afe410fd.gitlab.io/)
 - [Emma Beuel](https://emma-beuel-fablab-ulb-enseignements-2023-2024-fa-aa17017aa12728.gitlab.io/)
 - [Edwin Van Landuyt](https://edwin-vanlanduyt-fablab-ulb-enseignements-2023-2-d1a3492ebb6b17.gitlab.io/)

 Notre équipe est donc composé de trois bioingénieurs et d'un biologiste. Comme vous pouvez le voir la pluridisciplinarité n'est pas notre plus grande force mais au final ça ne s'est pas relevé comme un problème. Nous étions tous les 4 prêt à apprendre de nouvelles choses et à sortir de notre zone de confort pour ce projet. Evidemment certaines tâches aurait été réalisé plus rapidement par des personnes d'autres facultés, mais nous avons pris ça comme un défi à relever !


## L'abstract de notre projet 
Nous nous sommes posés comme simple et unique question : comment peut-on à notre échelle améliorer la vie de personnes souffrant au quotidien d'une pathologie ? En effet, il est difficile de s'imaginer que de simples actions de la vie de tous les jours telles que boire, manger, ouvrir une porte,.. peuvent être de réelles épreuves. Nous nous sommes dirigés plus particulièrement vers les personnes atteintes de tremblements chroniques. Nous avons découvert une importante communauté de personnes souffrant de ce trouble qui témoigne de l'impact physique et psychologique de celui-ci sur leur qualité de vie. Dès lors, notre projet Key-Z a vu le jour. C'est une solution à l'une des actions les plus importantes et simples du quotidien : rentrer chez soi. Cet objet a été réalisé de façon à prendre en compte 3 choses essentielles :

 1. Orienter
 2. Maintenir
 3. Alourdir

 Ces 3 points représentent les piliers de tout objet aidant réellement à atténuer les tremblements.

 Nous espérons par ce projet qu'ouvrir sa porte va désormais redevenir une action facile au quotidien pour cette communauté de personne. 

 ![Alt text](docs/images/417088490_383561177585964_7412973271059711960_n.jpg)