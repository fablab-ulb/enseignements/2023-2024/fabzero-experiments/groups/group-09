# Développement de notre projet
Voici la page qui vous permettra de suivre la réalisation de notre projet final pas à pas 😉 Bonne lecture !

## 1. Dynamique de groupe

La réalisation d'un projet en groupe s'accompagne d'une organisation complexe et d'une répartition des tâches. Lors des différents modules du cours, nous avons appris certaines méthodes de dynamique de groupe que nous allons pouvoir mettre à l'oeuvre pour ce projet. 

#### Comment le groupe fonctionne?

La réalisation d'un projet peut sembler une montagne si on ne le déconstruit pas en plusieurs échelons. Au sein de notre groupe, nous adoptons des méthodes de développement spirale et la planification modulaire et hiérarchique. Cela consiste à diviser le projet en modules hiérarchisés sur une échelle d'importance. Ainsi, nous avançons par étapes et commençons par les plus élémentaires pour un projet final cohérent.

Nous avons très vite établit un plan d'organisation. Chaque semaine, chacun réalise des tâches individuellement. Chaque jeudi nous nous retrouvons en réunion pour la mise en commun, la prise de décision et la répartition des prochaines tâches. 


#### Déroulement d'une réunion type :

1. mise en commun des avancées de chacun (10mn)
2. bilan de la semaine (5mn)
3. espace de discussion (max 20mn)
4. prise de décision pour la fin de semaine et la semaine suivante (15mn)
5. vote par majorité (2mn)
6. répartition des tâches (3-4mn)
7. tour de parole afin de laisser la possibilité à chacun d'exprimer son ressenti sur la dynamique de groupe et le projet en géneral (pas de limite de temps)

Une fois la réunion terminée, nous demandons toujours l'avis de notre mentor sur les décisons qui ont été prises. En terme de communication, nous utilisons messenger pour discuter pendant la semaine et teams pour s'appeler et partager des documents. 

#### Outils de dynamique de groupe utilisés :
- Rôles : chaque semaine nous faisons tourner les rôles afin de laisser la possibilité à chacun d'expérimenter chaque rôle (animateur, secrétaire, gestion du temps et ambiance)
- Prise de décision : pour la prise de décision nous utilisons la technique des doigts motivés
- Vote : le vote se fait par majorité
- On utilise aussi le principe de méteo de sortie à la fin de chaque réunion


## 2. Problématique finale

À l'issu des 2 jours (6 et 7 novembre) de brainstorming intense, 3 problématiques sont ressortis : 

1. Comment améliorer la qualité de vie des personnes atteintes de la maladie de Parkinson ?
2. Comment trouver une manière de survivre aux conditions caniculaires qui nous attendent sans abuser de l'énergie ?
3. Comment améliorer la qualité de l'eau dans les pays en voie de développement ?


Après de nombreuses discussions et plusieurs votes, nous avons choisi de partir sur la première problématique : comment améliorer la qualité de vie des personnes atteintes de la maladie de Parkinson ?.

En réfléchissant et en faisant quelques recherches, nous nous sommes rapidement rendus compte que la problématique ne se limitait pas qu'aux personnes atteintes de la maladie de Parkinson. En effet, la forme la plus répandue de tremblement de main est le tremblement essentiel. En 2020, il concerne 1 personne sur 2000 (AVIQ et APTES-Belgique) et ses causes sont génetiques et neurologiques. Le choix de cette problématique nous a permis de découvrir que l'une de nos camarades du FabLab, Camille Lamon, est atteinte de ce type de tremblement. En discutant avec elle, nous avons vite compris que notre problématique était bel et bien réel. Des actions qui nous semblent simples à réaliser, comme boire ou manger, sont en faites de vraies difficultés pour elle au quotidien. Même des actions auxquelles nous n'aurions pas penser comme par exemple pincer une pipette au laboratoire de chimie est en faite extrêmement compliqué pour elle. 
 
Grâce à Camille, nous avons décider de reformuler notre problématique finale :  **Comment aider les personnes atteintes de tremblements chroniques à retrouver une autonomie au quotidien ?**

Nous avons également fait des recherches à côté et nous sommes tombés sur [cette page internet](https://www.aptes.org/au-quotidien/aide-technique/) qui a été créée par l'APTES (l'association des personnes concernées par le temblement essentiel). Nous avons vite compris que cette page est une manière de guider toutes personnes atteintes de ces tremblement en proposant des conseils, des techniques, et mêmes des parcours de soins. Il y a également un espace appelé "communauté" permettant aux gens d'échanger entre eux. Nous avons parcouru ce site rigoureusement et sommes tombés sur des recueils de témoignage et récits de vie qui nous ont permis de confirmer encore plus notre problématique. 

Une fois notre problématique finale décidée unanimement, nous nous sommes lancés dans la réalisation de notre arbre à problèmes et à solutions. 

#### Arbre à problèmes :

 ![Alt text](<images/arbre prob finale.jpg>)


#### Arbre à solutions :

![Alt text](<images/arbre à solution finale (2).jpg>)

Nous nous sommes aussi penchés sur les problèmes qu'il y a derrière notre problématique et nous nous sommes rendu compte qu'il y avait un réel manque de sensibilisation et de soutien aux personnes atteintes de tremblements. En effet, ces personnes ne se sentent pas toujours énormément soutenues et peuvent donc par la suite dévélopper de l'anxiété sociale, s'isoler, se priver de certaines activités ne se sentant pas capable d'y participer,... ce qui mentalement est d'autant plus dûr. Cela a donc plus d'impact qu'un simple manque d'autonomie chez ces personnes atteintes de tremblements.  


## 3. Brainstorming

Afin d'avancer dans la réalisation de notre projet, il est essentiel de commencer par étudier les contraintes et les ressources actuelles. En effet, partir tout droit sans réflechir est la meilleure manière de nous prendre un mur. Nous allons également cibler un problème/une situation bien précise que l'on veut résoudre de façon à ne pas partir dans tous les sens. 

#### Contraintes : 
Avant même de chercher des solutions à notre problème, nous avons déterminé les contraintes majeures qui en découlent. Ce sont des contraintes générales et pas encore adaptées à notre prototype final:

- manque de connaissance sur les pathologies des patients en question ainsi que de leurs traitements et thérapies
- prouesse créative pour se démarquer car il existe déjà énormément d'objets et de solutions
- trouver une situation/ un cas très précis
- budgets, moyens et temps limités
- trouver des "cobayes" pour tester nos prototypes
- créer un objet vraiment utile et non seulement qui fonctionne

#### Ressources :

Il a également fallu qu'on définisse les ressources que nous avions à dispostion. Tout d'abord il était essentiel de trouver des mentors pouvant nous guider dans la réalisation de notre projet. Ainsi, nous avons essayé de retracer les carrières professionnelles de nos familles et connaissances dans un premier temps. Un ergotherapeute et deux kinésitérapeuthes ont pu dès lors être contactés via Noa et Emma. Ensuite, nous allons aussi nous servir de sources internet, livres et conseils d'enseignants. Finalement, nous voulions rendre visite à des maisons de retraites afin d'étudier des cas précis et avoir le témoignage de personnes atteintes de tremblements chroniques. Maheureusement les nombreuses maisons de retraites que nous avons contactées, se sont avérées trop occupées ou pas en mesure de nous aider.

#### Problèmes précis : 

- **boire** : pour la plupart des personnes atteintes de tremblement, c'est un réel problème de boire sans renverser la moitié du contenu de son verre.
- **manger** : lorsque vous tremblez, vous n'êtes jamais sûrs à 100% que tout le contenu de votre nourriture arrivera à votre bouche.
- **se servir d'une cruche, d'une bouilloire** : se servir de l'eau froide ou chaude est une action particulièrement précise que même des gens non-atteints de tremblement trouvent laborieuse, imaginez donc en tremblant...
- **inserer sa clé dans la serrure** : insérer une clé dans une serrure est également une action qui devient compliquée lorsque vous avez une main qui tremble, vu la précision du geste. 
- **cliquer sur une souris d'ordinateur** : réaliser un seul clic sur une souris peut s'avérer difficile lorsque vous ne la contrôlez pas.


#### Prototypes Hackathon

Lors du Hackathon s'étant déroulé le 13-14/11/2023, nous devions prototyper plusieurs solutions en seulement deux cours, et uniquement avec des outils et des matériaux très simples (papier, carton, ciseaux, scotch,...). Voici les différents prototypes faits en classe :

 - Magic Spoon : une cuillère avec capuchon qui consiste à empêcher son contenu de tomber lors du trajet de l'assiette à la bouche.
 - Click Cinelle : une souris munie de quatre boutons, deux qui limitent les clics droites et gauches à un seul clic, et deux autres qui servent à faire des double-clics droits et gauches afin d'éviter des erreurs de clics dues à des tremblements.
 - Key Z : bracelet pouvant s'attacher au poignet afin de diminuer la difficulté à insérer la clé dans la serrure.
 - Verse-cruche : dispositif permettant de verser le contenu d'une cruche à l'aide d'un système à bascule pour réduire les risques de renversement.
 - Give me a hand : demandé par le prof, il fallait que l'un de nos prototypes soit un peu déjanté. On a donc imaginé des mains drones guidées par le cerveau via une puce qu'on insère dans la tête.


## 4. Solution finale (key-Z) et prototypes

Après mure réflexion et de longues discussions, nous avons décidé de développer l'idée de Key-Z avec plusieurs nouveaux prototypes. Tout d'abord parce que c'est l'un des rares problèmes où l'on a trouvé peu d'idées developpées sur le marché. Tous les autres problèmes identifiés ont déjà été exploités et certains avec des techniques assez pointues. Nous avons tous été également beaucoup inspirés par ce problème, un grand nombre de solutions ont émergées rapidement. 

Nous avons tout de même été parlés à Camille afin de lui demander si cela s'avérait un véritable problème pour elle. Elle nous a répondu qu'en effet c'était un problème, nous avons même eu le droit à une "démonstration" de sa part que vous pouvez observer sur la vidéo ci-dessous. 
Denis et d'autres assistants nous ont également conseillés de continuer sur cette problématique précise. 

Il est temps à présent de prototyper notre solution finale !  C'est une étape qui prend du temps et qu'il faut commencer au plus vite, pour avoir le temps d'échouer et de reprototyper afin d'arriver au résultat souhaité. 

### Réalisation de notre prototype finale 
Nous avons dans un premier temps établi la liste de contraintes à garder en tête :

Contraintes du prototype final (key-Z):

- Trouver un objet universel adapté à toutes les clés et serrures possibles,
- Réaliser un objet pas trop emcombrant, facilement transportable,
- Objet qui règle réellement la problématique. 

Etat de l'art :

Avant de commencer toute modélisation, nous avons pris le temps de regarder les différents objets déja crées pour résoudre cette problèmatique.

![Alt text](images/415140446_1041083033792606_1430334770099962972_n.jpg)

Nous avons été très surpris de réaliser que quelques unes de nos idées ont bel et bien déjà été crées. En effet l'une de nos premières idée etait celle de l'entonnoir (comme l'image du milieu) mais nous étions arrivés à la conclusion que l'on deplaçait le problème : insérer la clé reste compliqué même si l'espace d'insertion est réduit. 

Nous avions également pensé à l'idée du manche (première photo à gauche) mais nous étions tous d'accord pour affirmer que celui vendu sur le marché pouvait être amelioré. 

En effectuant différentes recherches sur quels concepts et outils attenueraient le mieux les tremblements de main, 3 choses essentielles sont apparues :

- L'objet doit créer un appui pour la personne afin de faciliter le mouvement de la main
- L'objet ne devra pas être trop fin afin d'augmenter la surface de prise 
- L'objet devra être lesté, au moins un minimum

(D.I.U. Européen de rééducation et d’appareillage en chirurgie de la main, 2015-2016)

#### Etape 1 : brainstorming + 1ères idées sur feuille papier

Nous avons durant les deux premières réunions pris le temps de discuter tous ensemble des idées de chacun. Quelques illustrations en sont resortis : 

![Alt text](images/410514167_308505678255768_5497858898798699198_n.jpg)

Nos 3 premières idées : 

- Pisto-clé : clé integrée dans un manche (maintenu par un aimant) qui permet à la personne atteinte de tremblement d'avoir une plus grande surface de prise afin de faciliter son mouvement. 

- Bras articulé avec ventouse pour appui : pour cette idée, nous étions tous les quatres à la recherche d'une manière pour proposer un système d'appui à la personne lorsqu'elle ouvre sa porte. Cependant nous avons vite réalisé que transporter dans son sac une ventouse portabe risquerait de poser un léger problème d'encombrement. 

- Pose bras (exosquelette) : le but ici serait de créer une sorte d'attelle qui aurait comme but de limiter les tremblements, pas qu'au niveau de la main et du poignet mais aussi du bras. En effet, en nous renseignant nous avons réalisé que les tremblements chez certaines personnes commençaient dès le début du bras (URML-Normandie, 2018). Il est donc intéressant de prendre en compte ce facteur-ci aussi. 


#### Etape 2 : rencontre avec des professionnels

Nous avons discuté avec Camille des 3 choses, pour nous, essentielles que notre prototype doit respecter afin de s'assurer que cela reflète bien la réalité. Elle nous a bien confirmé qu'en effet, pour améliorer les mouvements d'une personne atteinte de tremblement, il faut s'attaquer d'une part à la **surface de prise de l'objet** mais également au **poids de l'objet**. En effet un objet léger mais épais n'attenuera pas les tremblements, de même pour les objets lourds mais pas épais. Elle nous en a fait la démonstration avec d'une part un stylo fin où l'on voyait sa main trembler dans tous les sens versus un stylo épais, spécialement conçu pour diminuer les tremblements. La différence était frappante, sa main ne tremblait presque plus. Nous savions à ce moment là que nous avions touché juste. 

Nous sommes tout de même allés prendre un deuxième avis chez un kinésithérapeute. Ceui-ci nous a plutôt conseillé d'apporter une source d'appui à notre prototype. En effet, si l'objet est plus lourd mais que le mouvement s'effectue toujours sans un appui, le tremblement risque de ne pas être assez attenué. Les mouvements "dans le vide" sont ceux les plus durs à réaliser sans trembler. 

Nous sommes par la suite tombés sur une page internet, [Tous Ergo](https://www.tousergo.com/blog/lutter-contre-les-tremblements/), destinée aux personnes atteintes de tous types de tremblements. Leur devise afin de lutter contre les tremblements au quotidien : **Maintenir – Alourdir – Orienter**
Ceci nous a d'autant plus permis de prouver que nous avons relevé les points importants à solutioner avec notre prototype. 

### Etape 3 : Prototype 1 

Nous avons commencé à modéliser notre premier prototype relativement tard car nous avions tous énormément de travaux à rendre dans d'autre cours. Il était également aussi assez dur de se lancer et de visualiser la taille, la forme et l'aspect de ce premier prototype. De plus, nous n'étions encore pas totalement convaincus de l'efficacité de notre idée. 

Après quelques semaines de doute et de procrastination, nous nous sommes lancés et avons modélisés notre premier manche. Par la suite nous l'avons imprimé à l'imprimante 3D. 

![Alt text](images/manche_pro1.jpg)


Nous avons vite réalisé en le faisant tester à Camille que celui-ci n'était pas assez épais. Les trous pour les doigts ne semblaient pas non plus nécessaires, au contraire ceux-ci réduisaient la surface de prise. 
Nous avons donc réimprimé 2 autres manches : un épais avec les trous pour les doigts et un autre également épais mais sans trou afin de voir la différence. Sur ce dernier nous avons également ajouté une attache qui nous permettra de relier notre manche avec un système d'appui. 

![Alt text](images/mance_scad.jpg)

![Alt text](images/manche_imprime.jpg)

Le manche le plus épais sans trou était le plus efficace. 

Il a fallu par la suite réflechir à notre système d'appui. Ceci n'a pas été une mince affaire... insérer un tel système nous paraissait assez compliqué à imaginer. Il fallait d'un côté un système permettant la rotation de la clé mais aussi un système attaché au manche. 

C'est ainsi que l'idée du système à "3 bras" nous est parvenue. Ce système est muni de son propre système de rotation qui pourra donc tourner en même temps que le manche. Ce système de rotation consiste en une série de billes glissant à l'intérieur d'un cercle. Nous avons donc modélisé et imprimé le système de rotation ainsi que le système à "3 bras".

![Alt text](images/bras_scad.jpg)


La première impression du système de rotation fut un échec, les billes étaient trop petites et ne roulaient pas. La deuxième impression a été un succès et nous avons pû incorporer le système de rotation à notre système à "3 bras". 

![Alt text](images/414133408_748253006811188_6445442068843155206_n.jpg)

Après déjà de nombreuses heures d'impressions, il y avait toujours un problème légèrement important à régler : **On la met où et comment la clé ?** 

Nous avons donc décider d'ajouter une pièce qui permettra d'insérer la clé. Celle-ci sera directement reliée au mécanisme de rotation ansi qu'au manche. 

![Alt text](images/415352597_932217548465065_670363140859340223_n.jpg)

![Alt text](images/413967664_1553149755497032_5283490400727300522_n.jpg)

Dès lors, nous pouvons assembler les deux pièces ensemble afin de tester leur comptabilité. Honnêtement nous nous attendions à ce que les deux pièces ne s'attachent pas ensemble du premier coup à cause des mesures de chacune qui pouvaient ne pas être adaptées. Et pourtant...

![Alt text](images/assemblage.jpg)

Bingo ! Les deux pièces s'emboîtent, c'était sincérement la meilleure nouvelle de la semaine. Nous nous sommes par contre rendus compte que nous avions perdu énormement de temps dû à attendre des impressions de chaque pièce qui pouvaient parfois durer 6h (le manche). Ce temps perdu nous a rapproché très fortement du pré-jury, mais notre prototype n'etait toujours pas fini... 
Le système d'appui n'est pas complet et nous n'avons pas eu encore le temps de réflechir à un mécanisme pour caler une clé dans le trou de notre prototype. 

### Etape 4 : Pré-jury
Nous sommes arrivés au pré-jury avec un prototype mais...un prototype loin d'être complet et fonctionnel. Nous étions tous un peu déçu mais nous savions que nous avions fait le maximum pour quand même avoir quelque chose à présenter. Le temps d'impression de chaque pièce a été lourdement sous-estimé par toute l'équipe. Pour le pré-jury, nous avons tous dû préparer un power point illustrant notre projet. Chaque groupe a presenté son projet et ensuite pendant 2h le jury est passé dans chaque groupe afin de donner son avis. 

Remarques du jury : 

- Pièce trop encombrante
- Personne ne va transporter une pièce aussi encombrante avec lui

Conseils du jury :

- Détacher le système d'appui et le manche 
- Créer un système d'appui qui se fixerait directement sur la porte

Nous sommes resortis de ce pré-jury plutôt déçus mais surtout assez stressés car ils nous restaient encore énormement à faire et nous rentrions tous très bientôt en blocus. Nous avons essayé de prendre en compte les remarques du jury mais malheureusement réaliser une pièce moins encombrante reviendrait à revoir le projet de A à Z et ceci nous était totalement impossible par manque de temps... Nous avons donc décider de continuer sur les prototypes déjà crées afin de les terminer. 

Il est vrai que notre pièce n'est pas spécialement pratique mais en tout cas elle sera fonctionelle et surtout ergonomique. Notre objet sera adapté pour toutes personnes souffrant de tremblement.

### Etape 5 : Prototype 2 

Nous allons tout de même essayer de rendre notre modèle le plus pratique possible sans pour autant lui retirer ses proprietés ergonomiques. Le défi est donc de trouver le bon compromis. 

Après le pré-jury, il a tout de même fallu se répartir les tâches afin d'être le plus efficace possible : la moitié du groupe travaillait sur la documentation et l'autre moitié continuait de modéliser et d'aller faire tourner les impressions. Jusqu'à présent nous avons tous réussis à ne pas céder à la panique et l'ambiance génerale du groupe a toujours été très positive. Nous voulons tous la même chose : venir à bout du projet. 

Pour la réalisation de ce deuxième prototype, nous avions plusieurs tâches en tête très précises à réaliser : 

- réaliser un manche plus lourd 
- trouver le mécanisme d'insertion de la clé 
- finaliser le système d'appui
- ajuster chaque pièce au mécanisme final
- finaliser le modèle

#### 1. Réaliser un manche plus lourd 
Afin d'apporter un certain poids à notre manche nous allons y insérer du sable. Le but n'est évidemment pas d'avoir un manche de 2kg mais apporter un certain poids à un objet, même moindre, a un réel pouvoir atténuant sur les tremblements. Des sites comme celui de [l'APTES](https://www.aptes.org/au-quotidien/aide-technique/) et [Tous Ergo](https://www.tousergo.com/blog/lutter-contre-les-tremblements/) sont les premiers à recommander l'utilisation d'objet plus lourds au quotidien pour faciliter les mouvements. 

Nous avons d'abord réimprimé un manche auquel on a incorporé un trou et nous avons également modélisé et imprimé un capuchon adéquat pour refermer ce trou. 

![Alt text](images/415382226_776450474313071_5380672583530177652_n.jpg)

La première impression du bouchon était malheureusement ratée car il était un peu trop grand par rapport au trou dû à des défauts d'impression.

![Alt text](images/414461214_911877593837327_3141788976644384356_n.jpg)

La deuxième impression a été un succès, tout comme l'impression du nouveau manche ! 

![Alt text](images/415299631_290628756940451_4091262850579430286_n.jpg)


#### 2. Trouver le mécanisme d'insertion de la clé

Trouver un mécanisme pour que notre clé s'insère correctement, ne fût pas une tâche facile. Le véritable défi était de travailler sur l'efficacité du système en fonction de 3 contraintes majeures, car elles sont étroitement liées: 
- la taille: le système doit être petit car il doit rentrer dans un petit cylindre de quelques centimètres mais en même temps avoir une taille assez conséquente pour pouvoir soutenir le poids d'une clé, ce qui nous mène à la 2ème contrainte
- la pression exercée sur la clé: la force exercée sur la clé doit être supérieure à son poids. Sans s'aventurer trop dans des calculs de physique, il faut juste que la force aplliquée sur la clé grâce au ressort, élastique, aimant... puisse contrer son poids mais aussi les forces de frottements une fois que la clé est dans la serrure. Il faut aussi que la pression soit adaptable car les largeurs de clés sont très variées.
- la facilité d'emploi pour une personne atteinte de tremblements: c'est bien beau des systèmes comme décrits auparavant mais il faut que les personnes concernées puissent s'en servir! Donc il faut un système simple d'emploi qui ne requiert pas trop de précision sinon le problème est simplement déplacé.

A l'issue d'un brainstorming, nous avons identifié plusieurs éventuelles solutions:

- un système qui dépend de la tension d'un élastique
- un aimant
- un système basé sur un méchanisme adéquat 
- un système basé sur la déformabilité d'un ressort 
- de la mousse ou caoutchouc déformable

1. Le système "à dents":

![Alt text](images/cleaaa.png)

Description: 

Ici nous pouvons observer un prototype de système basé sur un ressort. Le méchanisme consiste à faire coulisser une tête (en rouge) liée à un ressort le long d'un socle avec des dents. Sur son parcours la tête va se bloquer entre les dents et ainsi bloquer la clé sur la paroi du cylindre (dans lequel la clé est insérée, ce sera décrit plus tard). Comme il y a plusieurs dents sur le socle le rapprochement de la tête à la paroi est adaptable en fonction de la largeur de la clé.
Au début, nous voulions tester ce méchanisme en double, c'est-à-dire de la disposer des 2 côtés du cylindre (dans leuqel la clé se trouve) pour la bloquer. Or nous nous sommes rendu compte que c'était plus simple de mettre le système sur un seul côté car la clé est de toute façon bloquée contre la paroi opposée.

Nous avons abandonné ce méchanisme car il ne remplissait pas les caractères mentionnés plus haut.
En effet, plus on réduisait la taille du système moins il était efficace en terme de pression car la taille de ressort choisi réduit aussi proportionnellement. On pourrait cela-dit avoir plusieurs ressorts collés et compressés pour plus de pression mais à ce moment là le petit système ne supporte pas la pression. La tête rouge ne reste plus collée au ressort. 


2. "Compliant mechanism":

 ![Alt text](images/cleb.png)

 Description:

Ce méchanisme fonctionne grâce à la déformation de 2 bras en plastique. La boite centrale comprend 4 petits cylindres sur ces coins afin de pouvoir emboiter les 2 bras flexibles. Les 2 bras sont positionnés parallèlement dans la boite et sont modèlisés de manière à ce que leur longueur soit supérieure à celle de la boîte pour que justement ils soient pliés. Au centre, on insère une "branche" qui va écraser les 2 bras parallèles flexibles et ainsi la branche va se bloquer d'un coté ou de l'autre du sommet des bras flexibles (qui forment 2 arcs qui se touchent). La branche comprend en son centre une forme plus large pour qu'elle se bloque d'un côté de l'arc ou de l'autre. Finalement, il y a un cube dans lequel la branche s'insère. Le cube est lui-même placé dans le cylindre où la clé se trouve. Et pour enboiter le tout, l'autre moitié de la boite centrale vient fermer le système.

Nous avons abandonné ce système car une fois de plus, il était trop grand pour l'emboiter dans notre prototype. Plus on réduisait la taille, plus il perdait en efficacité. Les bras en plastique ne peuvent pas assumer le poids de la clé. C'était aussi compliqué de trouver les bonnes dimensions pour que tout fonctionne.


![ ](<bloque clé1-1.png>) ![Alt text](bloquecl%C3%A92-1.png)

Nous nous sommes inspirés du méchanisme sur la photo ci-dessous.
![Alt text](image.png)

Dont on peut voir le tuto [ici](https://www.instructables.com/100-3D-Printed-Linear-Snap-Action-Mechanism/)


3. Le système à élastique :

 ![Alt text](images/clefinla.png)

 Description: 

Cela consiste à bloquer la clé contre la paroi du trou de sorte qu'elle puisse rester fixe à l'intérieur. Pour ce faire, on a modélisé un petit bloc qui servira à coincer la clé dans le trou. Le bloc a les bords arrondis pour qu'il se décale de lui-même quand on enfonce la clé dans le trou. Ce bloc est aussi muni d'un creux à l'une de ses extrémités pour y faire passer un élastique.

![](images/coince-cle.png)

L'élastique sera enroulé autour de l'extension et du bloc pour que la clé soit plaquée par le bloc grâce à la force élastique.

Au début, on avait décidé de faire un creux dans l'extension pour que l'élastique y passe.

![](images/cale_elastique_avant.png)

Malheureusement, l'élastique ne tenait pas en place dedans. On a donc changé le creux en support qui ressort de l'extension afin de le caler.

![](images/cale_elastique_apres.png)

Cette dernière impression a été un succès ! Voici ce que cela a donné :

![](images/cle_bloque.jpg)
![](images/elastique_position.jpg)

#### 3. Finaliser le système d'appui

Nous avons garder la base du système d'appui de notre premier prototype mais il fallait à présent le finaliser.


 ![Alt text](images/yeah.png)


Celui-ci fonctionne parfaitement mais nous n'avions pas encore pris en compte la taille des ressorts qu'il faudra justement insérer dans ces bras. 
Concernant les ressorts, nous avions d'abord crée des ressorts à l'aide de l'imprimante 3D mais très vite on s'est rendu compte que ceux-ci n'étaient pas du tout flexible, d'autant plus si l'on voulait créer de grand ressorts.
De plus, l'impression de ceux-ci étaient très compliqué, une fois sur deux elle ne marchait pas. 

![Alt text](images/414125333_687488100224578_9124267705009870184_n.jpg)

 Nous avons donc acheté différents ressorts sur internet et choisi ceux qui semblaient être les mieux adaptés à notre modèle

![Alt text](images/414157287_1080735306293741_3725829393220400382_n.jpg)

Nous avons dès lors changé la largeur des bras du système afin que les ressorts rentrent parfaitement dedans.
 
 ![Alt text](images/415280542_352977457687358_1237514961305056449_n.jpg)
 ![Alt text](images/415300585_861570355749461_8292941343041996156_n.jpg)

Voici ci-dessous les bâtonnets d'appui qui seront collés aux ressorts qui seront eux même collés au système d'appui multi-bras.

![Alt text](images/415536687_412853577740087_393403903954965934_n.jpg)

 Le système final se composera donc :

* du système d'appui multi-bras
* deux ressorts dans chaque bras afin que le système puisse s'adapter à différentes tailles de clé
* de bâtonnets d'appui 
* de mousse permettant une meilleure adhésion à la porte
 
 Afin de permettre au système de glisser sur la porte pour que la personne puisse le positionner correctement, nous avons décidé de coller de la mouche à l'extremité de chaque bâtonnet. Nous avions le choix entre 3 types de mousse (en partant de gauche à droite sur la photo) :
 
- Mousse à mémoire de forme : la mousse à mémoire de forme est un composé chimique à base de polyuréthane. Elle est mélangée à d'autres composés qui en augmentent la densité et la viscosité.
**Nous ne l'avons cependant pas utilisée** car elle elle prenait du temps à retrouver sa forme initiale

- Mousse haute résilience (HR) : la résilience est la capacité d'une matière à retrouver son aspect initial après l'exercice d'une pression. Les caractéristiques des mousses Haute Résilience permettent une utilisation intensive et cela pendant des années. Ces mousses sont exploitables dans les lieux publics grâce à son classement non feu.
**Nous avons choisis celle-ci** pour notre prototype, elle convenait parfaitement.

- Mousse polyéther : Les mousses polyéther sont des mousses synthétiques aux cellules ouvertes alvéolées.  Elles s’adaptent parfaitement aux capitonnages et aux couchages ou sièges d’appoint. Plus la densité est élevée, plus elle durera dans le temps.
**Nous n'avons pas utilisé celle-ci** parce que nous avions peur qu'avec le temps elle ne retrouve pas à chaque fois sa forme initiale. 

![Alt text](images/mousse.jpg) 

Nous avons par la suite collé notre mousse à l'extremité de chaque bâtonnet :

 ![Alt text](images/419229292_384742004092365_3686272959437802668_n.jpg)

## 5. Prototype final

Voici notre prototype final :
![Alt text](images/finalll.png)

Vous trouverez tous les codes de chacune des pièces dans la partie **"8. Annexes"**.

## 6. Dynamique de groupe final

Pour terminer, nous parlerons dans cette section des faiblesses et des forces de notre groupe ainsi que des problèmes auxquels nous avons fait face. 

Concernant **nos forces**, la principale était notre bonne cohésion. Nous nous sommes tous très bien entendus et avions une très bonne communication. En effet, chaque semaine on s'appelait pour s'organiser et répartir le travail entre nous. Etant donné la divergence de nos horaires respectifs, nous avons essayé de répartir le travail selon les disponibiltés de chacun en s'assurant que cela convienne à tout le monde. Après le pré-jury, nous devions encore beaucoup avancer sur notre projet et nous avons donc reparti le travail. Noa et Emma se sont occupées de la documentation, Merlin et Edwin du prototypage. Cela nous a permis d'être plus efficace en cette période d'examen, où le temps était assez compté. De plus, de nombreuses prises d'initiative ont été prises par chacun d'entre nous. Il n'a fallu courir après personne pour que le travail soit délivré, au contraire, des tâches ont été réalisées sans même l'avoir demandé. 

En ce qui concerne **nos faiblesses**, au plus nous avancions dans notre projet, aux plus nous étions confrontés à de nouveaux problèmes. Principalement, notre plus grosse faiblesse a été notre mauvaise gestion du temps. En effet, nous avions pris beaucoup de retard sur les différentes étapes de notre prototypage étant donné qu'on a dû prototypé beaucoup plus que prévu. De plus, notre projet dépendait majoritairement des imprimantes, ce qui nous rendait fortement dépendant du Fablab. Et donc, lorsque celui-ci était fermé, ce qui fût le cas lors des vacances de Noël et ce jusqu'au 8 janvier, nous n'avons pas pû avancer dans notre prototypage. Cela nous a donc fait perdre encore une fois beaucoup de temps. De plus, le temps d'impressions de chaque pièce a été également largement sous-estimé. Que ce soit nos grandes ou petites pièces, il fallait minimum compter 20mn d'impressions à chaque fois, tout en esperant que l'impression allait marcher du premier coup. Sinon on pouvait déjà recompter 20mn de plus et 20mn + 20mn + 20mn... vous êtes vite à 3-4h de perdues. Cela a donc été également un facteur qui nous a rendu moins efficace. 

Nous avons cependant toujours essayé de trouver des solutions afin de surmonter ces obstacles malgré la charge de travail qu'on avait tous en cette période d'examen. La dynamique est restée bonne jusqu`à la fin de ce projet. Personne n'a cédé à la panique et tout le monde a continué à faire sa part du boulot même en pleine session d'examen. De plus, lors des moments plus difficles d'inquiétude et de stress, nous trouvions toujours une manière de remonter le moral des uns et des autres afin d'être sur que tout le monde reste d'attaque. 

## 7. Conclusion

Pour conclure, nous pouvons affirmer que ce projet est loin d'avoir été de tout repos. **De nombreux facteurs n'ont pas été pris en compte** surtout en ce qui concerne la gestion du temps. Nous nous sommes aussi beaucoup de fois demandés si nous nous étions pas lancés dans une problématique trop compliquée à résoudre. Cependant, nous sommes au final tous très contents d'avoir été jusqu'au bout de notre idée. Nous n'avons rien lâché et avons tout de même réussi à réaliser un objet fonctionnel et ergonomique comme nous le souhaitions. Certes, nous savons que **celui-ci pourrait être encore largement amelioré** mais le but au final était de délivrer un concept qui pourrait, avec plus de temps et de moyens, passer au niveau supérieur. La première chose à faire évoluer serait bien évidemment sa **taille et sa fonctionnalité au quotidien**. Nous sommes cependant très contents d'avoir exploré cette problématique en fond et en large. Cela nous a permis de découvrir une communauté de personnes qui ne sont pas toujours prises en compte dans notre société. Nous espérons donc également que ce projet aura permis d'apporter une certaine **sensibilisation** à leur égard. Nous nous sommes aussi rendus compte que beaucoup d'autres choses pouvaient encore être créées et développées afin de faciliter leur vie au quotidien. **Dans quelques années, qui sait, peut-être que l'équipe Key-Z se réunira à nouveau afin de réfléchir à une nouvelle solution...** 

Un grand merci de l'équipe Key-Z à Denis et à l'équipe du Fablab pour leur temps et les moyens mis à notre disposition pour la réalisation de ce projet ! 

## 8. Annexes 

![Alt text](images/415500069_410206144904407_3451096093395986039_n.jpg)

![Alt text](images/systappuie_code.jpg)


![Alt text](images/manche_code.jpg)

![Alt text](images/415522472_872220081317585_5704272765109132217_n.jpg)



![Alt text](images/415500074_715925143937641_1119104353421239472_n.jpg)

![Alt text](images/417071596_338282249111094_1220603203320176759_n.jpg)

![Alt text](images/419929282_807764167779492_9211537826487064395_n.jpg)

![Alt text](images/batonnet_code.jpg)

![Alt text](<images/415470508_714278287474167_6922863306974720000_n (1).jpg>)

![Alt text](images/419371432_1500523243823609_3687664008567375445_n.jpg)

![Alt text](<images/piece attache.jpg>)

![Alt text](images/attache.jpg)

![Alt text](<412642911_1098404754813457_6005266304420315481_n (1).jpg>)